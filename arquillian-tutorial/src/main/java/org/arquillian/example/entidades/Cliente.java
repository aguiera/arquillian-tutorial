package org.arquillian.example.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@SequenceGenerator(name = "CLI_SEQ", sequenceName = "CLIENTE_SEQ", allocationSize = 1)

public class Cliente {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLI_SEQ")
	private Long id;
	
	@NotBlank
	private String nome;
	
	@org.hibernate.validator.constraints.br.CPF
	private String CPF;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cliente")
	List<Protocolo> protocolos = new ArrayList<Protocolo>();
	
	
	public Cliente() {
	
	}
	
	public Long getId() {
		return id;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCPF() {
		return CPF;
	}
	public void setCPF(String cPF) {
		CPF = cPF;
	}
	
	public void addProtocolo(Protocolo protocolo) {
		protocolo.setCliente(this);
		protocolos.add(protocolo);
	}
	
	public Protocolo getProtocolo(long numProtocolo) {
		
		//TODO Essa pesquisa é tosca. Tem que melhorar
		for (Protocolo protocolo : protocolos) {
			if(protocolo.getNumero() == numProtocolo) {
				return protocolo;
			}
		}
		
		return null;
	}
}
