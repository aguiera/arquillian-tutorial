package org.arquillian.example.entidades;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "PRO_SEQ", sequenceName = "PROTOCOLO_SEQ", allocationSize = 1)
public class Protocolo {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRO_SEQ")
	private long numero;
	private Date data;
	
	@ManyToOne
	private Cliente cliente;
	
	//List<Exame> exames = new ArrayList<Exame>();
	
	
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public long getNumero() {
		return numero;
	}
	public void setNumero(long numero) {
		this.numero = numero;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	
	/*public void addExame(Exame exame) {
		exames.add(exame);
	}
	
	public double total() {
		double soma = 0;
		
		for (Exame exame : exames) {
			soma += exame.getValor();
		}
		
		return soma;
	}*/
}
