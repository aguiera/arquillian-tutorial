package org.arquillian.example.servicos;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.arquillian.example.entidades.Cliente;
import org.arquillian.example.exceptions.AppException;


@Stateless
public class ClienteServicos {

	@PersistenceContext
	private EntityManager em;
	
	public Cliente inserir(Cliente cliente) {
		this.em.persist(cliente);
		return cliente;
	}
	
	public Cliente inserirComTreta(Cliente cliente) throws RuntimeException {
		this.em.persist(cliente);
		throw new RuntimeException();
		//return cliente;
	}

	public Cliente inserirComTretaForcado(Cliente cliente) throws RuntimeException {
		this.em.persist(cliente);
		throw new AppException();
		//return cliente;
	}

}
