package org.arquillian.example.exceptions;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = false)
public class AppException extends RuntimeException {

}
