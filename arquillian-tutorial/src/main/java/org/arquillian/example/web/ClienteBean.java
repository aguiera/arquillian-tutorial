package org.arquillian.example.web;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.arquillian.example.entidades.Cliente;
import org.arquillian.example.servicos.ClienteServicos;


@ManagedBean
@RequestScoped
public class ClienteBean {
	
	private Cliente cliente;
	
	@EJB
	private ClienteServicos clienteServicos;	
	
	public ClienteBean() {
		this.cliente = new Cliente();
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void eventoInserir() {
		this.clienteServicos.inserir(this.cliente);
		
		FacesMessage msg = new FacesMessage();
		msg.setSummary("Oh Que Lindo!!!!");
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

}
