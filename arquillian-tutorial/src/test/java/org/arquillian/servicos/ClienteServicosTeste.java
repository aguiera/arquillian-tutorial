package org.arquillian.servicos;

import javax.inject.Inject;

import org.arquillian.example.entidades.Cliente;
import org.arquillian.example.entidades.Exame;
import org.arquillian.example.entidades.Protocolo;
import org.arquillian.example.exceptions.AppException;
import org.arquillian.example.servicos.ClienteServicos;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class ClienteServicosTeste {

	@Deployment
	public static Archive<?> createDeployment() {
		WebArchive archive = ShrinkWrap.create(WebArchive.class, "arquilian-tutorial.war")
		
		//archive.addClasses(Protocolo.class, Cliente.class, Exame.class);
		//archive.addClass(AppException.class);
		//archive.addClass(ClienteServicos.class);
		//archive.addAsResource("META-INF/persistence.xml");
		//archive.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
		
		.addPackage(Cliente.class.getPackage())
		.addPackage(AppException.class.getPackage())
		.addPackage(ClienteServicos.class.getPackage())
		.addAsResource("META-INF/persistence.xml")
		.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
		
		System.out.println(archive.toString(true));
		
		return archive;
	}
	
	@Inject
	ClienteServicos clienteServicos;
	
	@Test
	public void test() {
		Cliente cliente = new Cliente();
		
		cliente.setNome("Anderson Guiera");
		cliente.setCPF("51881270670");
		
		clienteServicos.inserir(cliente);
		
		assertEquals((Long)1L, cliente.getId());
	}

}
